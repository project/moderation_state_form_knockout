INTRODUCTION
------------

When you have requirement that adding/editing any node and setting its workflow
state should be two distinct actions occurring on different site paths,
Moderation State Form Knockout allows the moderation state widget to appear
on node add/edit forms but disables interaction so that workflow changes can
occur elsewhere (e.g. using moderation controls on "Latest version" display).

## Rationale

Our initial plan to simply remove the moderation field widget from add/edit
form for each node type was discovered to be inadequate as it allows node edits
for published nodes to be saved as published.

This module uses form alter to:
1. remove non-draft options from the moderation state widget.
2. style the widget as disabled, so users are not tempted to try and alter
   the state.

## Other options

Setting field `#access` false prevents the widget from rendering, so behaviour
is like deleting widget via drupal form admin.
e.g. `$form['moderation_state']['#access'] = FALSE;`

Setting widget state select to disabled (a) removes it from render, so next
state is not visible, but also means widget value won't be submitted, e.g. 
`$form['moderation_state']['widget'][0]['state']['#type']['#disabled']
 = 'disabled';`

Setting `disabled="disabled"` via javascript removes field value from submitted
form values causing Drupal to complain about an invalid moderation state
transition.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core. It depends upon Content
Moderation, and it assumes workflow state `draft` exists.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

There is no configuration. When
enabled, the module will disable interaction with the moderation state
widget on node add/edit forms for all content types.
